package com.example.artifitial;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterContuct extends RecyclerView.Adapter<AdapterContuct.ContuctViewholder> {

    private Context context;
    private ArrayList<Model>arrayList;

    public AdapterContuct(Context context, ArrayList<Model> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ContuctViewholder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater layoutInflater =LayoutInflater.from(context);
        View view =layoutInflater.inflate(R.layout.recycler_item_view,parent,false);

        return new ContuctViewholder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final ContuctViewholder holder, final int i) {
        final Model userItem = arrayList.get(i);
        holder.imageView.setImageResource(arrayList.get(i).getImage());
        holder.name.setText(arrayList.get(i).getName());
        holder.phone.setText(arrayList.get(i).getPhone());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent yourIntent = new Intent(context, Contuct_details.class);
                Bundle b = new Bundle();
                b.putSerializable("user", userItem);
                yourIntent.putExtras(b); //pass bundle to your intent
                context.startActivity(yourIntent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class  ContuctViewholder extends RecyclerView.ViewHolder{
        TextView name,phone;
        ImageView imageView;
        public ContuctViewholder(@NonNull View itemView) {
            super(itemView);

            name=itemView.findViewById(R.id.name_id);
            phone=itemView.findViewById(R.id.phone_id);
            imageView=itemView.findViewById(R.id.imageiv);
        }
    }
}
