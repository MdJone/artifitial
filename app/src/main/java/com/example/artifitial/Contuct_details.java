package com.example.artifitial;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Contuct_details extends AppCompatActivity {
    TextView phone,name;
    Model user;
    ImageView imageView;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contuct_details);

        imageView=findViewById(R.id.image);
        name=findViewById(R.id.name);
        phone=findViewById(R.id.callnumber);
        try {

            Intent i = getIntent();
            Bundle bundle = i.getExtras();
             user = (Model) bundle.getSerializable("user");
             name.setText(user.getName());
             phone.setText(user.getPhone());
             imageView.setImageResource(user.getImage());
        }catch (Exception e){
            Log.e("error",e.toString());
        }

        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+user.getPhone()));
                startActivity(callIntent);
            }
        });
    }
}
