package com.example.artifitial;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ArrayList<Model>arrayList;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView=findViewById(R.id.recyclerview);

         arrayList=new ArrayList<>();
         arrayList.add(new Model("Mozibul hoq jone","01624874747",R.drawable.a));
         arrayList.add(new Model("Ariful hoq babu","01863908780",R.drawable.b));
         arrayList.add(new Model("Jahidul hoq kalam","0187654334",R.drawable.f));
         arrayList.add(new Model("Mirajul madamur","0187654321",R.drawable.d));
         arrayList.add(new Model("Sofiul Islam raju","01863908780",R.drawable.e));
         arrayList.add(new Model("Aminul Islam kasu","01863908780",R.drawable.f));
         arrayList.add(new Model("Mohobbat Ali tutu","01863908780",R.drawable.g));
         arrayList.add(new Model("Niyamot Ali balam","01863908780",R.drawable.h));
         arrayList.add(new Model("Soriful karim","01863908780",R.drawable.b));
         arrayList.add(new Model("Minhajur foring","01863908780",R.drawable.j));
         arrayList.add(new Model("Korimul hoq nuri","01863908780",R.drawable.k));
         arrayList.add(new Model("Imtiyaj suri","01863908780",R.drawable.b));

        AdapterContuct adapter=new AdapterContuct(MainActivity.this,arrayList);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

    }
}
